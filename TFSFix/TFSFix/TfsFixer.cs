﻿using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TFSFix
{
    public class TfsFixer
    {
        TFSFixOperationAbstract op;
        Stopwatch fStopWatch;
        TFSFixPArams fParams;
        TFSFIxResults fResutls;

        static string fLogFileName;
        public static readonly string TempPath = Path.Combine(System.IO.Path.GetTempPath(), "TFSFIX");

        public TfsFixer()
        {
            fStopWatch = new Stopwatch();
            
            string timeStamp = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            fLogFileName = String.Format("TfsFix_{0}.log", timeStamp);
            
            //Create temp directory to store files received from the TFS server
            if (!Directory.Exists(TempPath))
                Directory.CreateDirectory(TempPath);
        }

        

        public void Fix(TFSFixPArams aParams)
        {

            if (aParams is TFSFixCleanParams)
                op = new TFSDelteOperation(aParams as TFSFixCleanParams);
            else if (aParams is TFSSyncParams)
                op = new TFSSyncOperation(aParams as TFSSyncParams);

            fResutls = op.results;

            fParams = aParams;
            fStopWatch.Restart();

            Log("Start fixing...");

            //Get files
            string[] files = Directory.GetFiles(fParams.Path, "*", aParams.Recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
            
            //Filter files
            files = files.Where(x => !x.Contains(fParams.Exclude)).ToArray();

            TFSWorkspace workSpace;
            InitializeWorkspace(aParams, out workSpace);

            Workspace workspace = workSpace.workspace;
            VersionControlServer version = workSpace.version;


            //int latest = version.GetLatestChangesetId();
            //DateTime earliestDate = DateTime.Now.AddMonths(-1);
            //do
            //{
            //    Changeset changeset = version.GetChangeset(latest);
            //    Stream stream = changeset.Changes[0].Item.DownloadFile();
            //    StreamReader reader = new StreamReader(stream);
            //    string str = reader.ReadToEnd();


            //    if (changeset.CreationDate < earliestDate)
            //    {
            //        break;
            //    }
            //    // do analysis of the changeset here,
            //    // e.g. use changeset.Committer or changeset.Changes
            //} while (latest-- > 0);


            if (fParams.Threads > 1)
            {
                ParallelOptions options = new ParallelOptions();
                options.MaxDegreeOfParallelism = fParams.Threads;

                Parallel.ForEach(files, options, x =>
                    {
                        string fileName;
                        fileName = x;
                        ProcessFile(version, workspace, fileName);
                    }
                    );
            }
            else
            {
                foreach (string fileName in files)
                    ProcessFile(version, workspace, fileName);

            }
            fStopWatch.Stop();
            
            fResutls.TotalTime += fStopWatch.ElapsedMilliseconds;
        }

        private void InitializeWorkspace(TFSFixPArams aParams, out TFSWorkspace workspace)
        {
            workspace = new TFSWorkspace();
            Uri collectionUri = new Uri(aParams.Server);

            TeamFoundationServer server;
            if (String.IsNullOrEmpty(aParams.UserName) || String.IsNullOrEmpty(aParams.Password))
            {
                server = new TeamFoundationServer(aParams.Server);
            }
            else
            {
                NetworkCredential credential = new NetworkCredential(aParams.UserName, aParams.Password);
                server = new TeamFoundationServer(aParams.Server, credential);
            }

            workspace.version = server.GetService(typeof(VersionControlServer)) as VersionControlServer;
            WorkspaceInfo workSpaceInfo = Workstation.Current.GetLocalWorkspaceInfo(fParams.Path);

            // Try get workspace method 1
            if (workSpaceInfo != null)
            {
                workspace.workspace = workSpaceInfo.GetWorkspace(server);
            }

            else
            {             // No Workspace found using method 1, try to query all workspaces the user has on this machine.
                Workspace[] workspaces = workspace.version.QueryWorkspaces(null, Environment.UserName, Environment.MachineName);
                foreach (Workspace w in workspaces)
                {
                    foreach (WorkingFolder f in w.Folders)
                    {
                        if (fParams.Path.ToLower().Contains(f.LocalItem.ToLower()))
                        {
                            workspace.workspace = w;
                            break;
                        }
                    }
                }
            }


            if (workspace.workspace == null)
                throw new Exception("Could not find local workspace");

         
        }




        private void ProcessFile(VersionControlServer vcs, Workspace aWorkSpace, string aLocalFilePath)
        {
            fResutls.IncreaseValue("Files requested");

                try
                {
                    if (op.ProcessFile(vcs, aWorkSpace, aLocalFilePath))
                        fResutls.IncreaseValue("Files processed");  
                    
                }
                catch (Exception e)
                {
                    fResutls.IncreaseValue("Total errors");
                    op.NotifyMessageForFile("[ERROR]" , aLocalFilePath + Environment.NewLine + e.ToString());
                }
        }


        public static void Log(string message, bool writetoConsole = true)
        {
            if (writetoConsole)
                Console.WriteLine(message);
            try
            {
                File.AppendAllText(fLogFileName, message + "\n");
            }
            catch { }
        }
        public void PrintResults()
        {
            Log("");
            Log("========= RESULTS ====================");
            Log(fResutls.GetResultsAsString());
            Log("Total Time        : " + String.Format("{0:f2}", fResutls.TotalTime / 1000.0)  + " seconds");
            Log("=======================================");
        }

    
    }

}
