﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TFSFix
{

    class TFSFIxResults
    {
        private object lockObject = new object();
        private Dictionary<string, int> fProperties = new Dictionary<string, int>();
        public void IncreaseValue(string aName)
        {
            lock (lockObject)
            {

                int val;
                if (fProperties.TryGetValue(aName, out val))
                {
                    val++;
                    fProperties[aName] = val;
                }
                else
                    fProperties[aName] = 1;
            }

        }
       
        public long TotalTime { get; set; }

        public string GetResultsAsString()
        {
            int maxStringLength = -1;
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, int> pair in fProperties)
            {
                maxStringLength = Math.Max(pair.Key.Length, maxStringLength);
            }

            foreach (KeyValuePair<string, int> pair in fProperties)
            {
                
                sb.Append(pair.Key);
                sb.Append(' ', maxStringLength - pair.Key.Length + 1);
                sb.Append(": ");
                sb.Append(pair.Value.ToString());
                sb.Append(Environment.NewLine);
            }

            if (sb.Length > 0)
            {
                int lineFeedLength = Environment.NewLine.Length;
                sb.Remove(sb.Length - lineFeedLength, lineFeedLength);
            }
            return sb.ToString();
        }


    }
    class TFSFIxSyncResults : TFSFIxResults
    {
        

    }

    class TFSFIxDeleteResults : TFSFIxResults
    {

    }

}



