﻿using Microsoft.TeamFoundation.VersionControl.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSFix
{
    class TFSSyncOperation : TFSFixOperationAbstract
    {
        private TFSFIxSyncResults fResults;
        private TFSSyncParams fParams;
        public TFSSyncOperation(TFSSyncParams aParams) : base(aParams as TFSFixPArams)
        {
            fResults = new TFSFIxSyncResults();
            fParams = aParams;
        }


        override public bool ProcessFile(VersionControlServer vcs, Workspace aWorkSpace, string aLocalFilePath)
        {
            string serverFileName = aWorkSpace.GetServerItemForLocalItem(aLocalFilePath);
            int numProcessed = 0;
            if (vcs.ServerItemExists(serverFileName, ItemType.Any))
            {
                Item item = vcs.GetItem(serverFileName);
                

                bool isFilesConsideredIdentical = CompareLocalAndSeverFiles(aWorkSpace, item, aLocalFilePath);
                NotifyMessageForFile(isFilesConsideredIdentical ? "[SIMILAR]" : "[DIFFERENT]", aLocalFilePath);
               
                if (isFilesConsideredIdentical)
                {
                    fResults.IncreaseValue("Files the same");
                    if (!fParams.Simulate)
                        numProcessed = aWorkSpace.Undo(aLocalFilePath);
                }
                else
                {
                    fResults.IncreaseValue("Files different");
                    if (!fParams.Simulate)
                        numProcessed = aWorkSpace.PendEdit(aLocalFilePath);
                }

                return true;

            }
            else
            {
                if (fParams.AddUnversioned)
                    numProcessed = aWorkSpace.PendAdd(aLocalFilePath);

                NotifyMessageForFile("[UNVERSIONED]", aLocalFilePath);
                fResults.IncreaseValue("Files unversioned");
            }

            if (numProcessed == 0)
                throw new Exception("Could not complete operation");

            return false;
        }

        public override TFSFIxResults results
        {
            get
            {
                return fResults as TFSFIxResults;
            }
        }

        private bool IsFilesTheSame(BCCode aBCCode)
        {
            bool result = false;
            switch (fParams.ComparisonLevel)
            {
                case CompareLevel.BinaryIdentical:
                    result = aBCCode == BCCode.Binarysame;
                    break;
                case CompareLevel.None:
                case CompareLevel.RulesBasedSame:
                    result = aBCCode == BCCode.Rulesbasedsame
                             || aBCCode == BCCode.Binarysame
                              || aBCCode == BCCode.Similar;
                    break;
                default:
                    result = false;
                    break;
            }
            return result;
        }
        private bool CompareLocalAndSeverFiles(Workspace aWorkSpace, Item item, string aLocalFilePath)
        {
            bool result = false;
            if (fParams.ComparisonLevel == CompareLevel.BinaryIdentical)
            {
                //use direct memory compare
                Stream itemStream = item.DownloadFile();
                using (MemoryStream localFileMemoryStream = new MemoryStream(4 * 1024 * 1024))
                using (MemoryStream serverFilemorySteram = new MemoryStream(4 * 1024 * 1024))
                {

                    Stream localFileStream = File.Open(aLocalFilePath, FileMode.Open, FileAccess.Read);
                    localFileStream.CopyTo(localFileMemoryStream);
                    itemStream.CopyTo(serverFilemorySteram);

                    result = CompareMemoryStreams(localFileMemoryStream, serverFilemorySteram);

                }
            }

            else
            {
                //Use beyond compare
                string serverTempFilePath = Path.Combine(
                    TfsFixer.TempPath
                    , Path.GetFileNameWithoutExtension(aLocalFilePath) + "_" + item.ChangesetId.ToString() + "_" + item.ItemId.ToString()
                     );


                serverTempFilePath = Path.ChangeExtension(serverTempFilePath, Path.GetExtension(aLocalFilePath));


                item.DownloadFile(serverTempFilePath);
                BCCode bcResult = BCHelper.CompareFiles(aLocalFilePath, serverTempFilePath);
                BCHelper.CheckForError(bcResult);
                result = IsFilesTheSame(bcResult);
            }

            return result;
        }

        private static bool CompareMemoryStreams(MemoryStream ms1, MemoryStream ms2)
        {
            if (ms1.Length != ms2.Length)
                return false;
            ms1.Position = 0;
            ms2.Position = 0;

            var msArray1 = ms1.ToArray();
            var msArray2 = ms2.ToArray();

            return msArray1.SequenceEqual(msArray2);
        }

    }
}
