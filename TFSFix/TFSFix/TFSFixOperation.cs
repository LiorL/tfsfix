﻿using Microsoft.TeamFoundation.VersionControl.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSFix
{
    abstract class TFSFixOperationAbstract
    {
        private TFSFixPArams fParams;

        public TFSFixOperationAbstract(TFSFixPArams aParams)
        {
            fParams = aParams;

        }

        virtual public bool ProcessFile(VersionControlServer vcs, Workspace aWorkSpace, string aLocalFilePath)
        {
            return true;
        }

        virtual public TFSFIxResults results { get; }

        public void NotifyMessageForFile(string prefix, string aLocalFilePath)
        {

            TfsFixer.Log(prefix + ' ' + aLocalFilePath, fParams.ShowConsoleVerboseOutput);
        }
    }
}
