﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSFix
{
    public enum CompareLevel { None, BinaryIdentical, RulesBasedSame }
    public class TFSFixPArams
    {
        public bool Simulate { get; set; }
        public string Path { get; set; }
        public string Server { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public bool Recurse { get; set; }

        //public bool Async { get; set; }

        public TFSFixPArams()
        {
            Threads = 1;
            ShowConsoleVerboseOutput = true;

        }

        public int Threads { get; set; }

        public string Exclude { get; set; }
        
        public bool ShowConsoleVerboseOutput { get; internal set; }
    }

    public class TFSSyncParams : TFSFixPArams
    {
        public bool AddUnversioned { get; internal set; }
        public CompareLevel ComparisonLevel { get; set; }
    }

    public class TFSFixCleanParams : TFSFixPArams
    {
        private string fExtensions;

        private HashSet<string> fExtensionsSet;
        public bool ContainExtension(string aExtension)
        {
            return fExtensionsSet.Contains(aExtension);
        }

        public string CleanExtenstions {
            get
            {
                return fExtensions;
            }

            set
            {
                fExtensions = value;
                string[] strArray = fExtensions.Split(';');
                fExtensionsSet = new HashSet<string>();

                foreach (string str in strArray)
                    fExtensionsSet.Add(str.Trim(new char[] {' ','*','.' } ));
            }
                }

    }

    

}
