﻿using Microsoft.TeamFoundation.VersionControl.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSFix
{

    class TFSDelteOperation : TFSFixOperationAbstract
    {
        private TFSFIxDeleteResults fResults;
        private TFSFixCleanParams fParams;
        public TFSDelteOperation(TFSFixCleanParams aParams) : base(aParams as TFSFixPArams)
        {
            fParams = aParams;
            fResults = new TFSFIxDeleteResults();
        }

        override public bool ProcessFile(VersionControlServer vcs, Workspace aWorkSpace, string aLocalFilePath)
        {

            TFSFixCleanParams cleanParams = fParams as TFSFixCleanParams;

            if (cleanParams.ContainExtension(Path.GetExtension(aLocalFilePath)))
            {
                if (!cleanParams.Simulate)
                    aWorkSpace.PendDelete(aLocalFilePath);

                NotifyMessageForFile("[CLEAN]", aLocalFilePath);
                fResults.IncreaseValue("Deleted count");
                return true;
            }

            return false;

        }

        public override TFSFIxResults results
        {
            get
            {
                return fResults as TFSFIxResults;
            }
        }

    }
}
