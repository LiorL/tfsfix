﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSFix
{
    internal class BCHelper
    {
        static string fBeyondComparePath;
        static BCHelper()
        {
               fBeyondComparePath = FindBeyondCompare();
        }
        
        public static void CheckForError(BCCode result)
        {
            bool error = false;
            switch (result)
            {
                case BCCode.UnknownError:
                case BCCode.CannotFindBCompare:
                case BCCode.unableToWait:
                case BCCode.TrialPeriodExpired:
                case BCCode.ErrorLoadingScriptFile:
                case BCCode.ScriptSyntaxError:
                case BCCode.ScriptFailed:
                    error = true;
                    break;
                default:
                    error = false;
                    break;
            }

            if (error)
                throw new Exception("Error " + result.ToString());
        }

        //public static bool IsTheSame(BCCode result)
        //{
        //    bool error = false;
        //    bool isTheSame = false;

        //    switch (result)
        //    {
        //        case BCCode.Binarysame:
        //        case BCCode.Rulesbasedsame:
        //            isTheSame = true;
        //            break;
        //        case BCCode.Similar:
        //        case BCCode.Binarydifferences:
        //        case BCCode.Rulesbaseddifferences:
        //        case BCCode.ConflictsDetected:
        //        case BCCode.ConflictsDetectedNotWritten:
        //            isTheSame = false;
        //            break;
        //        case BCCode.Sucess:
        //        case BCCode.UnknownError:
        //        case BCCode.CannotFindBCompare:
        //        case BCCode.unableToWait:
        //        case BCCode.TrialPeriodExpired:
        //        case BCCode.ErrorLoadingScriptFile:
        //        case BCCode.ScriptSyntaxError:
        //        case BCCode.ScriptFailed:
        //            error = true;
        //            break;
        //        default:
        //            error = true;
        //            break;
        //    }

        //    if (error)
        //        throw new Exception("Error " + result.ToString());

        //    return isTheSame;
        //}


        public static BCCode CompareFiles(string fileName, string tempFilePath)
        {
            int result;
            Process myProcess = new Process();
            myProcess.StartInfo.UseShellExecute = false;
            // You can start any process, HelloWorld is a do-nothing example.
            myProcess.StartInfo.FileName = fBeyondComparePath;
            myProcess.StartInfo.CreateNoWindow = false;
            myProcess.StartInfo.Arguments = String.Format("/qc \"{0}\" \"{1}\"", tempFilePath, fileName);
            myProcess.Start();
            myProcess.WaitForExit();
            result = myProcess.ExitCode;
            return (BCCode)result;
        }

        public static string FindBeyondCompare()
        {
            string result = null;

            string programFiles32Bit =  Helper.ProgramFilesx86();

            string beyonComparePath = Path.Combine(programFiles32Bit, "Beyond Compare 4", "BComp.exe");
            if (File.Exists(beyonComparePath))
            {
                result = beyonComparePath;
            }

            else
            {
                beyonComparePath = Path.Combine(programFiles32Bit, "Beyond Compare 3", "BComp.exe");
                if (File.Exists(beyonComparePath))
                {
                    result = beyonComparePath;
                }
            }


            return result;
        }

    }
}
