﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.IO;
using TFSFix;

namespace TfsApplication
{
    class Program
    {

        static readonly string[] paths =        
        {
            
            //@"C:\1project\"
        @"C:\1project\SIMbox\common\"

            //@"C:\1project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\YodaApi"
            //Network render
            //@"d:\2project\Simbox\KnowBook\Simulation\3DEngine\common\NetworkRender\NetworkRenderHub\"
        
            //@"C:\1project\TeamBranches\Yoda\CDB\Simul_4.0.3\Yoda\Dependencies\SimulSDK\Simul"
        //CDB paths 
        //@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Terrain\CDBTerrainEngineLib\CDBTerrainEngine"
        //,@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Resource\GraphicEngine\Yoda\Resources\Packs\simigon_CDB"
        //@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\ogreDep\code\src\FreeImage\Source"
        
            //RTSS
            //@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Resource\GraphicEngine\Yoda\Resources\Packs\simigon_RTShaderSystemLibs"

        //Root path
        //@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine"

        //RTSS
        //@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\Components\RTShaderSystem"

//        ,@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\RenderSystems\Direct3D11"
        ////@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\build\Win32" 
        //@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\RenderSystems\GL3Plus"
        //@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk"
        //@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\Samples"
        //@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\Scripts"
        //@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\Docs"
        //@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\Tools"
        //@"D:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\Tools\MayaExport\mel"
        //,@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\Components"
       // ,@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\OgreMain"
        //,@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\PlugIns"
        //,@"d:\2project\SIMbox\KnowBook\Simulation\3DEngine\Yoda\Ogre\v1-8-trunk\RenderSystems"

    };
            

        static TfsFixer tfsFixer = new TfsFixer();

        static void Main(String[] args)
        {
            for (int i = 0; i < paths.Length; i++ )
            {
                tfsFixer.Fix(new TFSSyncParams()
                    {
                         UserName = "LiorL"
                        , Password = "Zz654321"
                        , Path = paths[i]
                        , Exclude = "$tf"
                        , Server = "http://simigon-server:8080/TFS"
                        , Recurse = true
                        , ComparisonLevel = CompareLevel.BinaryIdentical
                        , Threads = 16
                        , Simulate = false
                        , ShowConsoleVerboseOutput = true
                        , AddUnversioned = false
                }
                    );
                }

            tfsFixer.PrintResults();
        }
   
    }
}