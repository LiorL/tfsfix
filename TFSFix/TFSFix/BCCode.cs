﻿namespace TFSFix
{
    enum BCCode
    {
        Sucess = 0,

        Binarysame = 1,

        Rulesbasedsame = 2,

        Binarydifferences = 11,

        Similar = 12,

        Rulesbaseddifferences = 13,

        ConflictsDetected = 14,

        UnknownError = 100,
        /// <summary>
        /// Conflictsdetected, merge output not written 
        /// </summary>
        ConflictsDetectedNotWritten = 101,
        /// <summary>
        /// BComp.exe unable to wait until BCompare.exe finishes
        /// </summary>
        unableToWait = 102,
        /// <summary>
        /// BComp.exe cannot find BCompare.exe
        /// </summary>

        CannotFindBCompare = 103,

        TrialPeriodExpired = 104,

        ErrorLoadingScriptFile = 105,

        ScriptSyntaxError = 106,
        /// <summary>
        /// Script failed to load folders or files 
        /// </summary>
        ScriptFailed
    }
}